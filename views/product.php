<?php session_start(); ?>
<?php $title = "Product";?>
<?php include "../partials/template.php";?>

<?php function get_content(){ ?>

	<div class="container">
		<div class="row">
	<?php
		require "../controllers/connect.php";
			if(isset($_GET["id"])){
				$prod_id = $_GET["id"];
				$sql = "SELECT * FROM items WHERE id = '$prod_id'";
			}else{
				header("location: catalog.php");
		}

		$result = mysqli_query($conn,$sql);
		$row = mysqli_fetch_assoc($result);

		echo "<div class='card my-3 border-0'>
			  <div class='row no-gutters'>
			    <div class='col-md-3'>
			      <img src='$row[img_path]'>
			    </div>
			    <div class='col-md-9'>
			      <div class='card-body'>
			        <h5 class='card-title'>$row[name]</h5>
			        <p class='card-text'>$row[description]</p>
			        <h5>₱ $row[price]</h5>
			        <div>
				        <form method='POST' action='catalog.php?id=$prod_id'>
					     	<input class='form-control w-25 d-inline border border-secondary mr-1' type='number' name='qty' value='1' min='1'>
							<button class='btn btn-primary w-25' type='submit'><i class='fas fa-cart-plus mr-2'></i>Add to Cart</button>
						</form>
					</div>	
			      </div>
			    </div>
			  </div>
			</div>";

 	?>
</div>
</div>

<?php }?>