<?php session_start();?>
<?php $title = "Home";?>
<?php include "../partials/template.php";?>

<?php function get_content(){ ?>
	<!-- Insert Jumbotron -->
	<?php
		if(isset($_GET["message"])){
		$message = $_GET["message"];
		if($message == "1"){
		?>
			<script>
			$(document).ready(function(){
			$('#login-form').modal('show');
			$('.modal-body').prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert">Invalid credentials<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
			})
			</script>
		<?php
		}else if($message == "2"){
			?>
				<script>
				$(document).ready(function(){
				$('#login-form').modal('show');
				$('.modal-body').prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert">Please log in<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
				})
				</script>
			<?php
		}
	}
	?>
	<div class="jumbotron">
		<div class="container">
			<h1 class="dispay-3">The DEMO Shop</h1>
			<p>
				This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something unique.
			</p>
			<p>
				<a href="catalog.php" class="btn btn-outline-primary" role="button"><i class="fas fa-shopping-bag mr-2"></i>Shop Now<i class="fas fa-angle-double-right ml-2"></i></a>
			</p>
		</div>
	</div>
	<!-- Featured Items -->
	<div class="container">
	  <!-- Page Features -->
	  <h2>Featured Items</h2>
	  <hr>
	  <div class="row text-center">
	    <!-- Retrieve records from products table and display here using
	      class "card" in bootstrap
	      -->
	    <?php
	      require "../controllers/connect.php";
	      $sql = "SELECT * FROM items ORDER BY RAND() limit 4";
	      $result = mysqli_query($conn,$sql);
	      
	      if(mysqli_num_rows($result) > 0){
	        while($row = mysqli_fetch_assoc($result)){
	          echo "
	          <div class='col-md-3 mb-3'>
	      
	            <div class='card h-100'>
	               <img src='$row[img_path]'>
	                  <div class='card-body'>
	                    <h4 class='card-title'><a href='product.php?id=$row[id]'>$row[name]</a></h4>
	                    <h5>₱ $row[price]</h5>
	      
	                  <a href='product.php?id=$row[id]' class='btn btn-block btn-primary'><i class='fas fa-binoculars mr-2'></i>View Product</a>
	                  </div>
	      
	            </div>
	          </div>";
	        }
	      }
	      ?>
	  </div>
	</div>
	<!-- /.row -->
	</div>
	<!-- /.container -->
<?php }?>