<?php session_start();
	if(in_array("0" OR "", $_SESSION["cart"])){
		header("location: cart.php?message=2");
	}

	if(isset($_GET["id"])){
		$prod_id = $_GET["id"];
		unset($_SESSION["cart"][$prod_id]);
	
		if(count($_SESSION["cart"]) == 0){
			unset($_SESSION["cart"]);
		}
	}

	if(!isset($_SESSION["cart"])){
		header("location: cart.php");
	}

	if(!isset($_SESSION["fname"])){
		header("location: home.php?message=2");
	}
?>
<?php $title = "Checkout";?>
<?php include "../partials/template.php";?>

<?php function get_content(){ ?>
	<section class="container">
		<form>
			<div class="row mt-3">
				<div class="col col-md-8">
					<h3 class="mb-3">Shipping Information:</h3>
					<?php
					require "../controllers/connect.php";

					$email = $_SESSION["email"];
					$sql2 = "SELECT * FROM users WHERE email = '$email'";
					$query = mysqli_query($conn, $sql2);

					if(mysqli_num_rows($query) > 0){
						while($row = mysqli_fetch_assoc($query)){
							echo "<span style='font-size: 1.03rem;' class='text-dark'>First name:</span><input class='form-control border-secondary mb-4' type='text' name='fname' value='$_SESSION[fname]' required>";
							echo "<span style='font-size: 1.03rem;' class='text-dark'>Last name:</span><input class='form-control border-secondary mb-4' type='text' name='fname' value='$row[lname]' required>";
							echo "<span style='font-size: 1.03rem;' class='text-dark'>Address:</span><textarea class='form-control border border-secondary mb-4' rows='2' name='address' required>" . $row["address"] . "</textarea>";

						}
					}

					?>
					<button class="btn btn-info">Continue to Payment Information</button>
	        	</div>
	        	<div class="col col-md-4">
	        		<div class="card">
						<div class="card-header text-center pt-3">
							<h4>Order Summary:</h4>
						</div>
						<div class="card-body">
							<?php

								if(isset($_SESSION["cart"])){

									ksort($_SESSION["cart"]);

									$ids = implode(',',array_keys($_SESSION["cart"]));

									$sql = "SELECT * FROM items WHERE id IN ($ids)";

									$result = mysqli_query($conn,$sql);

									$qty = array_values($_SESSION["cart"]);

									for($i = 0; $i < count($_SESSION["cart"]); $i++){
										$row = mysqli_fetch_assoc($result);

										echo "<img class='imgthumb float-left mr-2' src=$row[img_path]>
											<div class='card-text mb-4 position-relative'>
												<span>$row[name]</span>
												<p class='mb-0'>Qty: <span class='qty'>$qty[$i]</span></p>
												<span class='price d-none'>$row[price]</span>
												₱<span class='sub'></span>
												<p class='position-absolute' style='top: 1px; right: 1px;'><a class='text-danger' href='checkout.php?id=$row[id]'>Remove</a></p>
											</div>";
									} 
						
								}
							?>
						</div>
						<div class="card-footer">
							<h5 class="text-right">Shipping: <span class="text-info">Free</span></h5>
							<h5 id="final" class="text-right">Total:</h5>
						</div>
			 		</div>
				</div>
	        </div>
        </form>
	</section>

	<script>
		let qty = document.getElementsByClassName("qty");
	    let price = document.getElementsByClassName("price");
	    let sub = document.getElementsByClassName("sub");
	    let final  = document.getElementById("final");

		function getTotals(){
		for(i = 0; i < qty.length; i++){
			let subtotal = parseFloat(qty[i].innerHTML) * parseFloat(price[i].innerHTML);
			sub[i].innerHTML = subtotal.toFixed(2);
		}
		let x = sub.length;
		let sum = 0;
		while(x--){
			if(qty[x].innerHTML == ""){
			    sub[x].innerHTML = 0.00.toFixed(2);
			    let total = sum += parseFloat(sub[x].innerHTML); 
			    final.innerHTML = "Total: ₱" + total.toFixed(2);
			}else{
			    let total = sum += parseFloat(sub[x].innerHTML);        
			    final.innerHTML = "Total: ₱" + total.toFixed(2);
				}
			}
		}

		getTotals()
	</script>
<?php }?>