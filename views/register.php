<?php session_start(); ?>
<?php $title = "Register";?>
<?php include "../partials/template.php";?>

<?php function get_content(){ ?>
	<section class="container">
		<div class="card border-0 mt-2 mb-3 p-1">
			<div class="card-body">
				<h3 class="card-title text-center">Create an account</h3>
				<form method="POST" action="../controllers/register_action.php">
					<span style="font-size: 1.03rem;" class="text-dark">First name:</span> <input class="form-control border border-secondary mb-4" type="text" name="fname" required>
					<span style="font-size: 1.03rem;" class="text-dark">Last name:</span> <input class="form-control border border-secondary mb-4" type="text" name="lname" required>
					<span style="font-size: 1.03rem;" class="text-dark">Address:</span> <textarea class="form-control border border-secondary mb-4" rows="2" name="address" required></textarea>
					<div class="mb-4">
					<span style="font-size: 1.03rem;" class="text-dark">Email:</span> <input id="newmail" class="form-control border border-secondary" type="email" name="email" required>
					<small id="emailok"></small>
					</div>
					<div class="mb-4">
					<span style="font-size: 1.03rem;" class="text-dark">Password:</span> <input id="pw1" class="form-control border border-secondary" type="password" name="pword" required>
					<small id="passok1"></small>
					</div>
					<div class="mb-4">
					<span style="font-size: 1.03rem;" class="text-dark">Re-type password:</span> <input id="pw2" class="form-control border border-secondary" type="password" required>
					<small id="passok2"></small>
					</div>
					Already have an account? <a class="text-info" href="#" data-toggle="modal" data-target="#login-form">Log in here</a>
					<div class="text-right">
						<button onclick="clearAll()" type="reset" class="btn btn-outline-primary">Clear</button>
						<button type="submit" class="btn btn-primary" id="register">Submit</button>
					</div>
				</form>
			</div>
	</section>
<script>
	 $("#newmail").keyup(function(){
    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(this).val())){
        $.post("../controllers/check_email.php", {email: $(this).val()}, function(data){
            if(data == "unavailable"){
				$("#newmail").removeClass("is-valid")
				$("#newmail").addClass("is-invalid")
				$("#emailok").removeClass("text-success")
				$("#emailok").addClass("text-danger")
				$("#emailok").html("Email is unavailable")
            }else if(data == "available"){
				$("#newmail").removeClass("is-invalid")
				$("#newmail").addClass("is-valid")
				$("#emailok").removeClass("text-danger")
				$("#emailok").addClass("text-success")
				$("#emailok").html("Email is available")
            }
        })
    }else{
		$("#newmail").removeClass("is-valid")
		$("#newmail").removeClass("is-invalid")
		$("#emailok").html("")
    }
});

	$("#pw1, #pw2").keyup(function(){
		if($("#pw1").val() == ""){
			$("#pw1, #pw2").removeClass("is-valid")
			$("#pw1, #pw2").removeClass("is-invalid")
			$("#passok1").html("")
			$("#passok2").html("")
		}else{
			if($("#pw1").val().length < 8){
				$("#pw1").removeClass("is-valid")
				$("#pw1").addClass("is-invalid")
				$("#passok1").removeClass("text-success")
				$("#passok1").addClass("text-danger")
				$("#passok1").html("Password does not have at least 8 characters")
				var clearance = "invalid"
			}
			if(/\s/.test($("#pw1").val())){
				$("#pw1").removeClass("is-valid")
				$("#pw1").addClass("is-invalid")
				$("#passok1").removeClass("text-success")
				$("#passok1").addClass("text-danger")
				$("#passok1").html("Password has one or more spaces")
				var clearance = "invalid"
			}
			if($("#pw1").val() !== $("#pw2").val()){
				$("#pw2").removeClass("is-valid")
				$("#pw2").addClass("is-invalid")
				$("#passok2").removeClass("text-success")
				$("#passok2").addClass("text-danger")
				$("#passok2").html("Passwords don't match")
			}else{
				$("#pw2").removeClass("is-invalid")
				$("#pw2").addClass("is-valid")
				$("#passok2").removeClass("text-danger")
				$("#passok2").addClass("text-success")
				$("#passok2").html("Passwords match")
			}
			if(clearance !== "invalid"){
				$("#pw1").removeClass("is-invalid")
				$("#pw1").addClass("is-valid")
				$("#passok1").removeClass("text-danger")
				$("#passok1").addClass("text-success")
				$("#passok1").html("Password is valid")
			}
		}
	})

	$("#register").on("click", function(e){
		if($("#pw1").hasClass("is-invalid") || $("#pw2").hasClass("is-invalid") || $("#newmail").hasClass("is-invalid")){
			$("form").addClass("animated shake")
			setTimeout(function(){
				$("form").removeClass("animated shake")
			}, 500)
			e.preventDefault()
		}
	})

	function clearAll(){
		$("#pw1, #pw2").removeClass("is-valid")
		$("#pw1, #pw2").removeClass("is-invalid")
		$("#passok1").html("")
		$("#passok2").html("")
	}
</script>
<?php }?>