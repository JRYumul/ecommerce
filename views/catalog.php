<?php session_start(); ?>
<?php
	if(isset($_GET["id"])){
		$prod_id = $_GET["id"];
		$qty = $_POST["qty"];
		if(!isset($_SESSION["cart"])){
			$_SESSION["cart"] = array();
			$_SESSION["cart"][$prod_id] = $qty;
		}else{
			$_SESSION["cart"][$prod_id] = $qty;
		}
	}
?>

<?php $title = "Catalog";?>
<?php include "../partials/template.php";?>

<?php function get_content(){ ?>
	<div class="container">
	  <div class="row">
	  	<!-- First Column -->
	    <div class="col-lg-2">
	      <h4 id="catalogue">Collection</h4>
	      <div class="list-group">
			<a class='list-group-item list-group-item-action' href='catalog.php?cat_id=$row[id]'><i class='far fa-dot-circle mr-2'></i>$row[name]</a>"
	 	 </div>
	    </div>
		<!-- Second column -->
	    <div class="col-lg-10">
	      <div class="form-group">
	        <div class="form-group">
	          <div class="input-group mb-3">
	            <input type="text" class="form-control border-secondary" id="search">
	            <div class="input-group-append">
	              <span class="input-group-text"><i class="fas fa-search"></i></span>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="row">
	      <?php
	      	require "../controllers/connect.php";
	      	  if(isset($_GET["cat_id"])){
	      	  	$cat_id = $_GET["cat_id"];
	      	  	$sql = "SELECT * FROM items WHERE category_id = '$cat_id'";
	      	  }else{
	      	 	$sql = "SELECT * FROM items";
	      	  }

		      $result = mysqli_query($conn,$sql);
		      
		      if(mysqli_num_rows($result) > 0){
		        while($row = mysqli_fetch_assoc($result)){
		          echo "
		          	<div class='col-md-4 mb-3'>
			            <div class='card h-100'>
			               <img src='$row[img_path]'>
			                  <div class='card-body'>
								<h4 class='card-title'><a href='product.php?id=$row[id]'>$row[name]</a></h4>
			                    <h5>₱ $row[price]</h5>
			                  <a href='product.php?id=$row[id]' class='btn btn-block btn-primary'><i class='fas fa-binoculars mr-2'></i>View Product</a>
			                  </div>
			      		</div>
		            </div>";
		        }
		      }
	      ?>
	      </div>
	    </div>
	  </div>
	</div>
<?php }?>