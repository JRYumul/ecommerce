<?php session_start(); ?>
<?php $title = "My Cart";?>
<?php include "../partials/template.php";?>

<?php function get_content(){ ?>
    <?php if(isset($_GET["message"])){
        $message = $_GET["message"];
        if($message == "1"){
            $message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Item removed<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>';
        }else if($message == "2"){
            $message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Item quantity must be at least 1<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>';
        }
        }else{
            $message = "";
        }
        ?>
    <section class="container">
        <style>
            .table th{
                padding: 15px;
            }
        </style>
        <h3 id="emptycart" class="text-center"></h3>
        <div class="row mt-4" id="cartform">
            <div class="col col-md-8">
                <h3 class="mb-3">My Cart</h3>
                <?php
                    echo $message;
                ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Item</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price</th>
                            <th scope="col">Sub-Total</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        require "../controllers/connect.php";

                        if(isset($_SESSION["cart"])){

                            ksort($_SESSION["cart"]);

                            $ids = implode(',',array_keys($_SESSION["cart"]));

                            $sql = "SELECT * FROM items WHERE id IN ($ids)";

                            $result = mysqli_query($conn,$sql);

                            $qty = array_values($_SESSION["cart"]);

                            for($i = 0; $i < count($_SESSION["cart"]); $i++){
                                $row = mysqli_fetch_assoc($result);

                                echo "<tr>
                                <th scope='row'><img class='imgthumb' src=$row[img_path]></th>
                                <td>
                                    <a href='product.php?id=$row[id]'>$row[name]</a>
                                    <input type='number' name='id' value='$row[id]' hidden>
                                </td>
                                <td>
                                    <input type='number' value='$qty[$i]' min='1' class='form-control border-secondary qty'>
                                </td>
                                <td>₱<span class='price'>$row[price]</span></td>
                                <td>₱<span class='sub'></span></td>
                                <td>
                                    <a href='../controllers/delete_item.php?id=$row[id]' class='btn btn-sm btn-outline-danger'>Remove</a>
                                </td>
                                </tr>
                                ";
                            } 
                
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col col-md-4">
                <div class="card">
                    <div class="card-header text-center pt-3">
                        <h4>Order Summary:</h4>
                    </div>
                    <div class="card-body text-center p-5">
                        <h5 id="final">Total:</h5>
                        <h5>Shipping: <span class="text-info">Free</span></h5>
                    </div>
                    <div class="card-footer">
                        <a href="checkout.php" id="coBtn" class="btn btn-block btn-primary mr-5">Check Out</a>
                    </div>
                </div>
                <?php
                    if(!isset($_SESSION["fname"])){
                ?>
                    <script>
                        $("#coBtn").on("click", function(e){
                            e.preventDefault();
                            $('#login-form').modal('show');
                        })
                    </script>
                <?php

                    }
                ?>
            </div>
        </div>
    </section>

    <script>
    let qty = document.getElementsByClassName("qty");
    let price = document.getElementsByClassName("price");
    let sub = document.getElementsByClassName("sub");
    let button = document.getElementsByClassName("btn-outline-secondary");
    let subBtn = document.getElementById("subBtn");
    let final  = document.getElementById("final");
    let x = sub.length;

    if(x == 0){
        $("#cartform").addClass("d-none")
        $("#emptycart").html("Your cart is empty. <a href='catalog.php' class='text-danger'>Continue shopping.</a>")
        $("#emptycart").addClass("mt-5")
    }

    function getTotals(){
        for(i = 0; i < qty.length; i++){
            let subtotal = parseFloat(qty[i].value) * parseFloat(price[i].innerHTML);
            sub[i].innerHTML = subtotal.toFixed(2);
        }
        let x = sub.length;
        let sum = 0;
        while(x--){
            if(qty[x].value == ""){
                sub[x].innerHTML = 0.00.toFixed(2);
                let total = sum += parseFloat(sub[x].innerHTML); 
                final.innerHTML = "Total: ₱" + total.toFixed(2);
            }else{
                let total = sum += parseFloat(sub[x].innerHTML);        
                final.innerHTML = "Total: ₱" + total.toFixed(2);
            }
        }
    }

    getTotals()

    function removeItem(){
        this.parentNode.parentNode.remove();
        let x = sub.length;
        let sum = 0;
        while(x--){
                let total = sum += parseFloat(sub[x].innerHTML);        
                final.innerHTML = "Total: P" + total.toFixed(2);
            }
    }

    for(let i = 0; i < qty.length; i++){
        qty[i].addEventListener("change", getTotals);
    }

    for(let i = 0; i < button.length; i++){
        button[i].addEventListener("click", removeItem);
    }

    $('.qty').on('change', function (event) {
        $.ajax({
        url: '../controllers/update_cart_action.php',
        type: 'POST',
        data: {id: $(this).parent().prev().find('input').val(), qty: $(this).val()}
        });
    })
    </script>
<?php }?>