<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <div class="container">
    <a class="navbar-brand" href="home.php"><i class="fab fa-buysellads fa-2x"></i> DEMO Shop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="catalog.php"><i class="fas fa-book"></i> Catalog
          <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="cart.php"><i class="fas fa-shopping-cart"></i> Cart
            <?php
              if(isset($_SESSION["cart"])){
                echo '<span class="badge badge-pill badge-primary">';
                echo count($_SESSION["cart"]);
                echo '</span>';
              }
            ?>

          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fas fa-info-circle"></i> About Us</a>
        </li>
<?php
             if(isset($_SESSION["fname"])){
               echo '<li class="nav-item">
                       <a class="nav-link" href="#"><i class="fas fa-user"></i> '. $_SESSION["fname"] .' </a>
                     </li>
                     <li class="nav-item">
                       <a class="nav-link" href="../controllers/logout.php"><i class="fas fa-sign-out-alt"></i> Log Out</a>
                     </li>';
             }else{
               echo '<li class="nav-item">
                       <a class="nav-link" href="#" data-toggle="modal" data-target="#login-form"><i class="fas fa-user-lock"></i>Log In</a>
                     </li>
                     <li class="nav-item">
                       <a class="nav-link" href="register.php"><i class="fas fa-user-plus"></i> Register</a>
                     </li>';
             }
           ?>
      </ul>
    </div>
  </div>
</nav>

