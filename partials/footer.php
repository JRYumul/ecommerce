<footer class="py-5 bg-dark container-fluid position-absolute" style="bottom: 0;">
  <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
  </div>
  <!-- /.container -->
</footer>

<div class="modal fade" id="login-form" tabindex="-1" role="dialog" aria-labelledby="login-form-label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content fast">
			<div class="modal-header">
				<h5 class="modal-title" id="login-form-label">Log in to Demo Shop</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="POST" action="../controllers/login_action.php">
					<div class="form-group p-2 mb-0">
						<span>Email address:</span>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1"><i class="far fa-envelope"></i></span>
							</div>
							<input class="form-control border-secondary" type="email" name="email" required><br>
						</div>
						<span>Password:</span>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1"><i class="fas fa-key"></i></span>
							</div>
							<input class="form-control border-secondary" type="password" name="pw" required><br>
						</div>
					</div>
				</div>
					<div class="modal-footer">
						<button class="btn btn-block btn-primary" type="submit">Log in</button>
						<p id="regtext">Don't have an account? <a class="text-info" href="register.php">Register here</a></p>
					</div>
				</form>
			</div>
	</div>
</div>

<!-- <script>
	let loc = window.location.href;
	if(/product/.test(loc) || /cart/.test(loc) || /checkout/.test(loc) || /catalog/.test(loc)){
    $("footer").addClass("fixed-bottom");
	};
</script> -->