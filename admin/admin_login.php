<?php
	if(isset($_GET["message"])){
		$message = $_GET["message"];
		if($message == "1"){
			$message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Please log in<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			 	</button></div>';
		}else if($message == "2"){
			$message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Invalid credentials<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  		 <span aria-hidden="true">&times;</span>
		  		</button></div>';
		}else if($message == "3"){
			$message = '<div class="alert alert-success alert-dismissible fade show" role="alert">' . "You've been logged out" . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    			<span aria-hidden="true">&times;</span>
 				 </button></div>';
		}
	}else{
		$message = "";
	}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/acc6c30d64.js" crossorigin="anonymous"></script>
    <!-- Bootswatch -->
    <link href="https://bootswatch.com/4/lux/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- Popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Admin Login</title>
  </head>
  <body>

	<section class="container">
		<h1 class="mt-5">Admin Login</h1>
		<?php
			echo $message;
		?>
		<form method="POST" action="admin_login_action.php">
			<div class="form-group">
				<span>Email address: </span><input class="form-control border-secondary" type="email" placeholder="Enter email" name="uname" required><br>
				<span>Password: </span><input class="form-control border-secondary" type="password" placeholder="Enter password" name="pw" required><br>
				<button class="btn btn-outline-primary" type="reset">Clear</button>
				<button class="btn btn-primary" type="submit">Log In</button>
			</div>
		</form>

	</section>
  </body>
</html>