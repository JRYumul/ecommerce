<?php
	session_start();

	if(!isset($_SESSION["username"])){
		header("location: admin_login.php?message=1");
	}
	require "../controllers/connect.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/acc6c30d64.js" crossorigin="anonymous"></script>
    <!-- Bootswatch -->
	<link href="https://bootswatch.com/4/lux/bootstrap.min.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="../../assets/css/style.css" rel="stylesheet">
    <!-- JS DataTables -->
    <link href="../../assets/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- Popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- JS DataTables JS -->
    <script src="../../assets/js/jquery.dataTables.min.js"></script>
    <title>Admin Page</title>
  </head>
  <body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
	<div class="container">
		<a class="navbar-brand" href="admin.php">DEMO Shop - Admin Page</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
				<a class="nav-link" href="../views/home.php"><i class="fas fa-home"></i> Home</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" href="../views/catalog.php"><i class="fas fa-book"></i> Catalog</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" href="admin_logout.php"><i class="fas fa-sign-out-alt"></i> Log Out</a>
				</li>
				</li>
			</ul>
			</div>
		</div>
	</nav>
  	<section class="container mb-4">
		<div class="border border-primary p-4 mt-2">
			<h3 class="text-center mb-4">Add Products</h3>
			<button class="btn btn-primary btn-block" data-toggle="modal" data-target="#addModal">Add</button>
		</div>
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="addModalLabel">Add Item</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
					<form method="POST" action="add_item_action.php">
						<span>Name:</span> <input class="form-control border border-secondary mb-2" type="text" name="pname" required>
						<span>Price:</span> <input class="form-control border border-secondary mb-2" type="number" name="price" required>
						<span>Description:</span> <textarea class="form-control border border-secondary mb-2" rows="3" name="desc" required></textarea>
						<span>Image URL:</span> <input class="form-control border border-secondary mb-3" type="text" name="imgpath" required>
						<p class="mb-1">Category:</p> <select class="form-control border-secondary" name="cat_id">
						<?php
						$sql1 = "SELECT * FROM categories";

						$query = mysqli_query($conn, $sql1);

						if(mysqli_num_rows($query) > 0){
							$num = 0;
							while($row = mysqli_fetch_assoc($query)){
								$num++;
								echo "<option value=$row[id]>$row[name]</option>";
							}
							}else{
								echo "No records";
							}
						?>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
				</form>
				</div>
			</div>
		</div>

		<h3 class="my-4">Product List</h3>
		<?php

			$sql = "SELECT * FROM items";

			$result = mysqli_query($conn, $sql);

			echo '<table id="itemTable" class="table table-hover">
					<thead>
						<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">Actions</th>
						</tr>
					</thead>
					<tbody>';

			if(mysqli_num_rows($result) > 0){
				$num = 0;
				while($row = mysqli_fetch_assoc($result)){
					$num++;
					echo "<tr>
							<td>$num</td>
							<td><a href='../views/product.php?id=$row[id]'>$row[name]</a></td>
							<td><a href='#' data-toggle='modal' data-target='#editModal' data-id='$row[id]'>Edit</a> | <a href='delete_item_action.php?id=$row[id]'>Delete</a></td>
						</tr>";
				}

				echo "</tbody>
				</table>";
			}else{
				echo "No records";
			}
		?>


	<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="editModalLabel">Edit Item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
				<form method="POST" action="edit_item_action.php">
					<input type="number" name="item_id" id="idedit" hidden>
					<span>Name:</span> <input class="form-control border border-secondary mb-2" type="text" name="pname" id="nameedit">
					<span>Price:</span> <input class="form-control border border-secondary mb-2" type="number" name="price" id="priceedit">
					<span>Description:</span> <textarea class="form-control border border-secondary mb-2" rows="3" name="desc" id="descedit"></textarea>
					<span>Image URL:</span> <input class="form-control border border-secondary mb-3" type="text" name="imgpath" id="urledit">
					<p class="mb-1">Category:</p> <select class="form-control border-secondary" name="cat_id" id="catedit">
					<?php

					$sql1 = "SELECT * FROM categories";

					$query = mysqli_query($conn, $sql1);

					if(mysqli_num_rows($query) > 0){
						$num = 0;
						while($row = mysqli_fetch_assoc($query)){
							$num++;
							echo "<option value=$row[id]>$row[name]</option>";
						}
						}else{
							echo "No records";
						}
					?>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Edit</button>
			</div>
			</form>
			</div>
		</div>
	</div>

	</section>
	<footer class="py-5 bg-dark">
	  <div class="container">
	    <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
	  </div>
  <!-- /.container -->
	</footer>
  </body>
<script>
	$(document).ready(function(){
		$('#itemTable').DataTable();
	});
	
	$('#editModal').on('show.bs.modal', function (event) {
		$.ajax({
    	url: 'get_record.php',
    	type: 'POST',
    	data: {id: $(event.relatedTarget).data('id')},
    	success: function(response){ 
			let data = JSON.parse(response);
			let x = data.category_id
    		$('#idedit').val(data.id);
   			$('#nameedit').val(data.name);
    		$('#priceedit').val(data.price);
			$('#descedit').html(data.description);
   			$('#urledit').val(data.img_path);
			$('#catedit').find('option:selected').removeAttr('selected')
			$('#catedit option[value=' + x + ']').attr('selected','selected');
			}
		});
	})

	$('#editModal').on('hide.bs.modal', function(){
		$('#idedit').val("");
		$('#nameedit').val("");
		$('#priceedit').val("");
		$('#descedit').html("");
		$('#urledit').val("");
		$('#catedit').find('option:selected').removeAttr('selected')
	})
</script>
</html>
