<!DOCTYPE html>
<html>
<head>
      <title>Demo</title>
</head>
<body>
<h1>Add Product</h1>
<form method="POST" action="#">
      Type Product: <input type="text" name="productName" id="productName">
      <span id="feedback"></span>
</form>
</body>
</html>

<script>
	let productName = document.querySelector("#productName");

	const formData = new FormData()

	productName.addEventListener("keyup", function(){
		let pNameVal = this.value;
		formData.append("pNameVal", pNameVal);

		// Fetch API
		fetch("items.php", {
			method: "POST",
			body: formData 
		})
		.then(function(response){
			return response.text()
		})
		.then(function(text){
			let feedback = document.querySelector('#feedback')
			feedback.innerHTML = text

		})
		.catch(function(error){
			console.log(error)
		})
	});
</script>